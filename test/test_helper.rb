require_relative './../../../test/test_helper'
require 'fakeweb'

FakeWeb.allow_net_connect = false

class ActiveSupport::TestCase
  include ActionDispatch::TestProcess
  self.use_transactional_fixtures = false
end

module ActivityIntegrationHelpers
  def callback_defined?(group, type, value, result=false)
    TimeEntry.send(group).each { |cb| result = true if cb.instance_variable_get('@filter').eql?(value) and cb.kind.eql?(type) }
    result
  end

  def action_helper(action, pm_id=nil)
    UndeliveredActivity.delete_all
    case action
      when 'create'
        FakeWeb.register_uri(:post, "http://padrino.whaleberry.com/activity/create", body: {status: 'ok'}.to_json, status: ["200", "OK"])
        Issue.first.project.update(send_notify: true)
        time_entry = Issue.first.time_entries.create(user: User.second, hours: 3, comments: 'test', spent_on: Time.now)
        assert_equal pm_id, time_entry.reload.pm_activity_id, 'id not equal or not present'
      when 'update'
        FakeWeb.register_uri(:patch, "http://padrino.whaleberry.com/activity/update", body: {status: 'ok'}.to_json, status: ["200", "OK"])
        Issue.first.project.update(send_notify: true)
        time_entry = Issue.first.time_entries.last
        time_entry.update(hours: 5, comments: 'test2')
        assert_equal [5, 'test2'], [time_entry.reload.hours, time_entry.reload.comments]
      when 'destroy'
        FakeWeb.register_uri(:delete, "http://padrino.whaleberry.com/activity/destroy", body: {status: 'ok'}.to_json, status: ["200", "OK"])
        time_entry = Issue.first.time_entries.last
        time_entry.destroy
        assert time_entry.destroyed?, 'TimeEntry not destroyed'
    end
  end
end
