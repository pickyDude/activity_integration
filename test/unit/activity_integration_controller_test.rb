require_relative '../test_helper'

class ActivityIntegrationControllerTest < ActionController::TestCase
  fixtures :projects, :users, :email_addresses, :user_preferences, :members, :member_roles, :roles,
           :groups_users,
           :trackers, :projects_trackers,
           :enabled_modules,
           :versions,
           :issue_statuses, :issue_categories, :issue_relations, :workflows,
           :enumerations,
           :issues, :journals, :journal_details,
           :custom_fields, :custom_fields_projects, :custom_fields_trackers, :custom_values,
           :time_entries

  test 'update_time_entry with valid params' do
    time_entry = TimeEntry.first; id = rand(999..9999)
    get :update_time_entry, activity_id: time_entry, id: id
    assert_equal id, time_entry.reload.pm_activity_id, 'pm_activity_id not match'
    assert_response :success
  end

  test 'update_time_entry with invalid params' do
    get :update_time_entry, activity_id: 3000
    assert_response 403
  end

  test 'update_notify_accept with valid params' do
    project = Project.first
    patch :update_notify_accept, name: project.identifier, val: true
    assert project.reload.send_notify, 'send_notify not equal true'
    assert_response :success
  end

  test 'update_notify_accept with invalid params' do
    patch :update_notify_accept, name: 'Some'
    assert_response 403
  end
end
