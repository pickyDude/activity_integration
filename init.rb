require_dependency '../lib/activity_integration'
require_dependency '../lib/hooks'

Redmine::Plugin.register :activity_integration do
  name 'Activity Integration plugin'
  author 'Someman'
  description 'This is a plugin for Redmine'
  version '0.0.3'
  url 'https://bitbucket.org/_someman/activity_integration'
  author_url 'http://example.com/about'
  permission :activity_integration, { activity_integration: [:update_time_entry, :update_notify_accept] }, public: true
end

if (Object.const_get(:TimeEntry).is_a?(Class) rescue false)
  TimeEntry.class_eval do
    before_save    :check_state
    after_destroy  :check_state
    after_commit   :send_activity

    def send_activity
      if self.project.reload.send_notify
        define_ids
        ActivityIntegration.send_activity(self)
      end
    end

    def check_state
      ActivityIntegration.activity_action = case
      when self.destroyed?
        'destroy'
      when self.new_record?
        'create'
      when !self.new_record?
        'update'
      end
      define_ids
    end

    def define_ids
      ActivityIntegration.activity_id ||= self.id unless self.new_record?
      ActivityIntegration.pm_activity_id ||= self.pm_activity_id if self.pm_activity_id.present?
    end
  end
end
