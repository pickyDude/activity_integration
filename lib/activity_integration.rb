module ActivityIntegration
  @pm_api_url = 'http://pm.nasctech.com/api/v1/custom_objects/redmine_api/activity'
  @notify_api_url = 'http://padrino.whaleberry.com/activity/'
  @undelivered_api_url = 'http://padrino.whaleberry.com/activity/undelivered'

  class << self
    attr_accessor :activity, :activity_action, :activity_id, :pm_activity_id

    def serialize(entry)
      {
        action: activity_action,
        project: entry.project.name,
        date: entry.spent_on,
        user: entry.user.login,
        comment: entry.comments,
        hours: entry.hours
      }
    end

    def send_activity(entry)
      @entry = entry
      resend if UndeliveredActivity.all.reload.any?
      self.activity = serialize(@entry)
      self.activity["id"] = self.pm_activity_id if self.pm_activity_id.present?
      response = (JSON.parse(eval(self.activity_action)) rescue {})
      check_result(response)
      self.pm_activity_id, self.activity_id = nil, nil
    end

    def create
      RestClient.post @pm_api_url, {
        activity: self.activity, token: 'W5Zsv5gRqrp68Khp3H1d'
      } rescue false
    end

    def update; create end

    def destroy
      RestClient.post @pm_api_url, {
        activity: { id: self.activity["pm_activity_id"] }, token: 'W5Zsv5gRqrp68Khp3H1d'
      } rescue false
    end

    def check_result(response)
      unless response["status"]&.eql?('ok')
        self.activity["activity_id"] = self.activity_id
        self.activity["error"] = response["mess"]
        unless (RestClient.post @undelivered_api_url, { activity: self.activity } rescue false)
          self.activity["delivery_type"] = 'undelivered'
          self.activity[:pm_activity_id] = self.activity.delete("id")
          UndeliveredActivity.create(self.activity)
        end
      else
        if self.activity_action.eql?('create')
          TimeEntry.find(self.activity_id).update_column(:pm_activity_id, response["mess"])
          self.activity[:id] = response["mess"]
        end
        self.activity[:task] = "#{@entry.issue.id} #{@entry.issue.subject}"
        unless (choose_action(self.activity.except("activity_id", "delivery_type")) rescue false)
          self.activity["delivery_type"] = 'notify'
          self.activity[:pm_activity_id] = self.activity.delete(:id)
          UndeliveredActivity.create(self.activity)
        end
      end
    end

    def resend
      UndeliveredActivity.all.each{|a|
        response =
        if a.delivery_type.eql?('undelivered')
          RestClient.post @undelivered_api_url, { activity: a.attributes.except("id", "delivery_type") } rescue false
        elsif a.delivery_type.eql?('notify')
          choose_action(a.attributes.except("id", "delivery_type", "error", "activity_id")) rescue false
        end
        UndeliveredActivity.delete(a) if response
      }
    end

    def choose_action(data)
      case self.activity_action
        when 'create'
          RestClient.post "#{@notify_api_url}#{self.activity_action}", { activity: data }
        when 'update'
          RestClient.patch "#{@notify_api_url}#{self.activity_action}", { activity: data }
        when 'destroy'
          RestClient.delete "#{@notify_api_url}#{self.activity_action}/#{data[:id]}"
      end
    end
  end
end
