module ActivityIntegration
  class ActivityIntegrationHook < Redmine::Hook::ViewListener
    render_on :view_projects_show_right, partial: 'projects/show'
  end
end
